---
title: Ferramentas WEB

language_tabs: # must be one of https://git.io/vQNgJ
  - html
  - css
  - javascript
  - c

toc_footers:
  - Ariel Sam Ribeiro
  - Guilherme Sandmann
  - Lucas Trancoso
  - Marcos G. Zimmermann

includes:
  - exercise1
  - exercise2
  - exercise3
  - exercise4
  - exercise5
  - exercise6
  - exercise7
  - exercise8

search: true
---

# Introdução
Resolução de exercícios do Manfred da matéria de Introdução a Programação usando Javascript.

Ferramentas utilizadas para o desenvolvimento dessa aplicação:

* Framework para construção de páginas estáticas [Middleman App](https://middlemanapp.com).
* Template em estilo documentação [Slate](https://spectrum.chat/slate).
