(function(doc){
  'use strict';

  function Exercise6(parentSelector) {
    this.textfield = doc.querySelector(parentSelector + ' [data-js="number"]');
    this.button = doc.querySelector(parentSelector + ' [data-js="check"]');
    this.result = doc.querySelector(parentSelector + ' [data-js="result"]');

    function showResult(number) {
      var complement;
      if (number == 0) {
        complement = 'neutro';
      } else if (number > 0) {
        complement = 'positivo';
      } else {
        complement = 'negativo';
      }
      this.result.innerHTML = '<strong>' + number + '</strong> é um numero ' + complement
    }

    function handleClick(event) {
      event.preventDefault();
      if (!this.textfield.value) return;

      var num = new Number(this.textfield.value);
      showResult.call(this, num);
    }

    function initialize() {
      this.button.addEventListener('click', handleClick.bind(this), false);
    }

    initialize.apply(this);
  }

  doc.addEventListener('DOMContentLoaded', function() {
    var ex6 = new Exercise6('[data-js="ex6"]');
  });

})(document);
