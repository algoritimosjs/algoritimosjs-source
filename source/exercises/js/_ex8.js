(function(doc){
  'use strict';

  function Exercise8(parentSelector) {

    function initialize() {
      // Initialize system
    }

    initialize.apply(this);
  }

  doc.addEventListener('DOMContentLoaded', function() {
    var ex8 = new Exercise8('[data-js="ex8"]');
  });

})(document);
