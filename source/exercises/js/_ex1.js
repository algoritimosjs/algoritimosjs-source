(function(doc){
  'use strict';

  function Exercise1(parentSelector) {
    this.note1 = doc.querySelector(parentSelector + ' [data-js="note1"]');
    this.note2 = doc.querySelector(parentSelector + ' [data-js="note2"]');
    this.note3 = doc.querySelector(parentSelector + ' [data-js="note3"]');
    this.result = doc.querySelector(parentSelector + ' [data-js="result"]');
    this.button = doc.querySelector(parentSelector + ' [data-js="calculate"]');

    function _showMedian(e){
      e.preventDefault();

      var val1 = new Number(this.note1.value);
      var val2 = new Number(this.note2.value);
      var val3 = new Number(this.note3.value);

      var total = [val1, val2, val3].reduce(function(total, value) {
        return total + value;
      });
      var avg = (total / 3);
      this.result.innerText = "O valor da media é: " + avg.toFixed(2);
      if (avg == 10) {
        this.result.innerHTML += '. Voce está <strong>Aprovado com Distinção!</strong>';
      } else if (avg >= 7) {
        this.result.innerHTML += '. Voce está <strong>Aprovado</strong>';
      } else {
        this.result.innerHTML += '. Voce está <strong>Reprovado</strong>';
      }
    }

    this.note1.addEventListener('keyup', _showMedian.bind(this), false);
    this.note2.addEventListener('keyup', _showMedian.bind(this), false);
    this.note3.addEventListener('keyup', _showMedian.bind(this), false);
    this.button.addEventListener('click', _showMedian.bind(this), false);
  }

  doc.addEventListener('DOMContentLoaded', function() {
    var ex1 = new Exercise1('[data-js="ex1"]');
  });

})(document);

