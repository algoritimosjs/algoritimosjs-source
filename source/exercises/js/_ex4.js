(function(doc){
  'use strict';

  function Exercise4(parentSelector) {
    this.form = doc.querySelector(parentSelector + ' [data-js="conversor-form"]');
    this.tbody = doc.querySelector(parentSelector + ' [data-js="conversor-wrapper"]');

    function clearForm() {
      this.form.word.value = '';
      this.form.word.focus();
    }

    function addRow(values) {
      event.preventDefault();

      var col1 = doc.createElement('td');
      col1.textContent = values.normal;
      var col2 = doc.createElement('td');
      col2.textContent = values.uppercase;
      var col3 = doc.createElement('td');
      col3.textContent = values.downcase;
      var col4 = doc.createElement('td');
      col4.textContent = values.reverse;
      var col5 = doc.createElement('td');
      col5.textContent = values.size;

      var docFrag = doc.createDocumentFragment();
      [col1, col2, col3, col4, col5].forEach(function(col) {
        docFrag.appendChild(col);
       })

      var tr = doc.createElement('tr');
      tr.setAttribute('title', 'Duplo clique para remover');
      tr.appendChild(docFrag);
      this.tbody.prepend(tr);
      clearForm.call(this);
    }

    function submitForm(event) {
      event.preventDefault();
      var word = this.form.word.value;
      if (!word) { this.form.word.focus(); return };

      var values = {
        normal: word,
        uppercase: word.toUpperCase(),
        downcase: word.toLowerCase(),
        reverse: word.split('').reverse().join(''),
        size: word.length,
     };

     addRow.call(this, values);
    }

    function removeRow(event) {
      event.target.parentNode.remove();
    }

    function initialize() {
      this.form.addEventListener('submit', submitForm.bind(this), false);
      this.tbody.addEventListener('dblclick', removeRow.bind(this), false);
    }

    initialize.apply(this);
  }

  doc.addEventListener('DOMContentLoaded', function() {
    var ex4 = new Exercise4('[data-js="ex4"]');
  });

})(document);
