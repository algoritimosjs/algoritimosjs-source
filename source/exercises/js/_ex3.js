(function(doc){
  'use strict';

  function Exercise3(parentSelector) {
    this.filterElems = doc.querySelectorAll(parentSelector + ' [data-js="table-filter"]');
    this.formElm = doc.querySelector(parentSelector + ' [data-js="add-contact-form"]');
    this.tbodyElm = doc.querySelector(parentSelector + ' [data-js="contacts-table-tbody"]');

    function clearForm() {
      this.formElm.nome.value = '';
      this.formElm.cidade.value = '';
      this.formElm.fone.value = '';
      this.formElm.email.value = '';
    }

    function addRow(values) {
      var col1 = doc.createElement('td');
      col1.textContent = values.name;
      col1.classList.add('info-nome');
      var col2 = doc.createElement('td');
      col2.textContent = values.city;
      var col3 = doc.createElement('td');
      col3.textContent = values.phone;
      col3.classList.add('info-fone');
      var col4 = doc.createElement('td');
      col4.textContent = values.email;

      var docFrag = doc.createDocumentFragment();
      [col1, col2, col3, col4].forEach(function(col) {
        docFrag.appendChild(col);
       })

      var tr = doc.createElement('tr');
      tr.setAttribute('title', 'Duplo clique para remover');
      tr.appendChild(docFrag);
      this.tbodyElm.appendChild(tr);
    }

    function submitForm(event) {
      event.preventDefault();
      var name = this.formElm.nome.value;
      if (!name) { this.formElm.nome.focus(); return };

      var values = {
        name: name,
        city: this.formElm.cidade.value,
        phone: this.formElm.fone.value,
        email: this.formElm.email.value
      }

      addRow.call(this, values);
      clearForm.call(this);
    }

    function removeRow(event) {
      event.target.parentNode.remove();
    }

    function filterTableRows(event) {
      var selector = event.target.getAttribute('data-filter-selector');
      var rows = doc.querySelectorAll(parentSelector + ' ' + selector);
      var term = event.target.value.toLowerCase();

      Array.from(rows).forEach(function(elm){
        if (elm.textContent.toLowerCase().includes(term)) {
          elm.closest('tr').classList.remove('invisivel');
        } else {
          elm.closest('tr').classList.add('invisivel')
        }
      });
    }

    function initialize() {
      var that = this;
      this.formElm.addEventListener('submit', submitForm.bind(this), false);
      this.tbodyElm.addEventListener('dblclick', removeRow.bind(this), false);
      Array.from(this.filterElems).forEach(function(elm) {
        elm.addEventListener('keyup', filterTableRows.bind(that), false);
      });
    }

    initialize.apply(this);
  }

  doc.addEventListener('DOMContentLoaded', function() {
    var ex3 = new Exercise3('[data-js="ex3"]');
  });

})(document);
