(function(doc){
  'use strict';

  function Exercise5(parentSelector) {
    this.input1 = doc.querySelector(parentSelector + ' [data-js="num1"]');
    this.input2 = doc.querySelector(parentSelector + ' [data-js="num2"]');
    this.button = doc.querySelector(parentSelector + ' [data-js="check"]');
    this.result = doc.querySelector(parentSelector + ' [data-js="result"]');
    this.template = {
      n1_eq_n2: '<code>n1 == n2</code>. Portanto <strong>Primeiro Numero</strong> é igual a <strong>Segundo Numero</strong>',
      n1_gt_n2: '<code>n1 > n2</code>. Portanto <strong>Primeiro Numero</strong> é maior que <strong>Segundo Numero</strong>',
      n2_gt_n1: '<code>n1 < n2</code>. Portanto <strong>Primeiro Numero</strong> é menor que <strong>Segundo Numero</strong>',
    }

    function interpolate(template, num1, num2) {
      template = template.replace(/n1/g, num1);
      template = template.replace(/n2/g, num2);
      return template;
    }

    function showResult(num1, num2) {
      var html;
      if (num1 > num2) {
        html = interpolate(this.template.n1_gt_n2, num1, num2);
      } else if (num2 > num1) {
        html = interpolate(this.template.n2_gt_n1, num1, num2);
      } else {
        html = interpolate(this.template.n1_eq_n2, num1, num2);
      }

      this.result.innerHTML = html;
    }

    function handleClick(event) {
      event.preventDefault();
      if (!this.input1.value || !this.input2.value) return;

      var num1 = new Number(this.input1.value);
      var num2 = new Number(this.input2.value);

      showResult.call(this, num1, num2);
    }

    function initialize() {
      this.button.addEventListener('click', handleClick.bind(this), false);
    }

    initialize.apply(this);
  }

  doc.addEventListener('DOMContentLoaded', function() {
    var ex5 = new Exercise5('[data-js="ex5"]');
  });

})(document);
