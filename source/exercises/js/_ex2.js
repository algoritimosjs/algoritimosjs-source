(function(doc){
  'use strict';

  function Exercise2(parentSelector) {
    this.timeElem = doc.querySelector(parentSelector +' [data-js="time"]');
    this.timezoneElem = doc.querySelector(parentSelector +' [data-js="timezone"]');
    this.resultElem = doc.querySelector(parentSelector +' [data-js="result"]');

    function selectTimezone(e) {
      var offset = new Number(e.currentTarget.value);
      var date = readTime.call(this);
      date.setHours(date.getHours() + offset);
      showTime.call(this, date)
    }

    function readTime() {
      var rawTime = this.timeElem.value;
      var fragments = rawTime.split(':');
      var h = new Number(fragments[0]);
      var m = new Number(fragments[1]);
      var s = new Number(fragments[2]);
      var date = new Date();
      if (h.valueOf()) date.setHours(h);
      if (m.valueOf()) date.setMinutes(m);
      date.setSeconds(s.valueOf() ? s : 0);
      return date;
    }

    function showTime(d) {
      this.resultElem.textContent = d.toLocaleTimeString();
    }

    function formattedCurrentTime() {
      var d = new Date();
      return d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    }

    function initialize() {
      this.timezoneElem.addEventListener('change', selectTimezone.bind(this), false);
      this.timeElem.value = formattedCurrentTime();
    }

    initialize.apply(this);
  }

  doc.addEventListener('DOMContentLoaded', function() {
    var ex2 = new Exercise2('[data-js="ex2"]');
  });

})(document);
