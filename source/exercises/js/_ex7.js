(function(doc){
  'use strict';

  function Exercise7(parentSelector) {
    this.link = doc.querySelector(parentSelector + ' [data-js="hello-world"]');

    function handleClick(event) {
      event.preventDefault();
      alert("Hello World");
    }

    function initialize() {
      this.link.addEventListener('click', handleClick, false);
    }

    initialize.apply(this);
  }

  doc.addEventListener('DOMContentLoaded', function() {
    var ex7 = new Exercise7('[data-js="ex7"]');
  });

})(document);
