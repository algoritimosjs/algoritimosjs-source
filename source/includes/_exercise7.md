# Exercício 7

> Código fonte e resolução do problema:

```html
<%= exercise_source 'ex7.html' %>
```

```javascript
<%= exercise_source 'js/ex7.js' %>
```

```css
<%= exercise_source 'css/ex7.scss' %>
```

```c
#include <stdio.h>

int main(void){
	printf("Hello World\n");
	return 0;
}

```

<aside class="notice">
  <strong>Aula 04 - Saída de dados</strong>
  <p>
    1. Faça um programa que imprima "Hello World"
  </p>
</aside>

<%= exercise_source 'ex7.html' %>
