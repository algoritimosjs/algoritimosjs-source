# Exercício 8

> Código fonte e resolução do problema:

```html
<%= exercise_source 'ex8.html' %>
```

```javascript
<%= exercise_source 'js/ex8.js' %>
```

```css
<%= exercise_source 'css/ex8.scss' %>
```

```c
#include <stdio.h>

int main(void){
	printf("--------------------------\n");
	printf("|   Manfred Heil Junior  |\n");
	printf("| manfred@example.com.br |\n");
	printf("|    +55 47 123123131    |\n");
	printf("--------------------------\n");
	return 0;
}

```

<aside class="notice">
  <strong>Aula 04 - Saída de dados</strong>
  <p>
    2. Construa um programa que mostre seus dados pessoais em tela como: nome, endereço, telefone, e-mail, CPF, RG, e outros...
  </p>
</aside>

<%= exercise_source 'ex8.html' %>
