# Exercício 1

> Código fonte e resolução do problema:

```html
<%= exercise_source 'ex1.html' %>
```

```javascript
<%= exercise_source 'js/ex1.js' %>
```

```css
<%= exercise_source 'css/ex1.scss' %>
```

```c
#include <stdio.h>

int main (void){
	float nota1, nota2, nota3, media;
	printf("Digite a nota 1: ");
	scanf("%f",&nota1);
	fflush(stdin);
	printf("Digite a nota 2: ");
	scanf("%f",&nota2);
	fflush(stdin);
	printf("Digite a nota 3: ");
	scanf("%f",&nota3);
	fflush(stdin);
	
	media=(nota1+nota2+nota3)/3;
	
	if (media == 10){
		printf("Aprovado com Distincao");
	}else if (media >= 7){
		printf("Aprovado");
	}else{
		printf("Reprovado");
	}
	
	return 0;
}
```

<aside class="notice">
  <strong>Aula 7</strong>
  <p>
		9) Faça um programa que peça três notas de um aluno e calcule a média. Analisar a média e imprimir uma das mensagens a seguir:
	</p>
  <ol>
    <li>a) A mensagem "Aprovado", se a média for maior ou igual a 7, com a respectiva média alcançada;</li>
    <li>b) A mensagem "Reprovado", se a média for menor do que 7, com a respectiva média alcançada;</li>
    <li>c) A mensagem "Aprovado com Distinção", se a média for igual a 10.</li>
  </ol>
</aside>

<%= exercise_source 'ex1.html' %>
