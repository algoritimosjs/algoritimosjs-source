# Exercício 3

> Código fonte e resolução do problema:

```html
<%= exercise_source 'ex3.html' %>
```

```javascript
<%= exercise_source 'js/ex3.js' %>
```

```css
<%= exercise_source 'css/ex3.scss' %>
```

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int n=0;
typedef struct agenda{
    char name[30];
    char fone[30];
}Agenda;

void novo(Agenda a[]){
    system("cls");
    printf("Nome: ");
    gets(a[n].name);
    fflush(stdin);
    printf("Telefone:  ");
    gets(a[n].fone);

    n++;
}

void imprimir(Agenda a[]){
    int i=0;
    system("cls");
    for(i=0;i<n;i++){
        printf("\n%io Nome: %s",i+1,a[i].name);
        printf("\tTelefone:  %s",a[i].fone);
    }
    getch();
}

void buscanome(Agenda a[]){
    system("cls");
    char procura[30];
    int i;
    printf("Nome para procurar: ");
    gets(procura);
    fflush(stdin);
    for(i=0;i<=n;i++){
         char *res; 
         res = strstr(a[i].name, procura);  
         if(stricmp(res, procura) >= 0){                
            printf("\nNome: %s",a[i].name);
            printf("\tTelefone: %s\n",a[i].fone);       
         }else{     
            printf("\nNao contem nenhum nome igual");
         }
    }
    getch();    
}

void buscafone(Agenda a[]){
    system("cls");
    char procura[30];
    int i;
    printf("Numero para procurar: ");
    gets(procura);
    fflush(stdin);
    for(i=0;i<=n;i++){
         char *res; 
         res = strstr(a[i].fone, procura);  
         if(strcmp(res, procura) >= 0){             
            printf("\nNome: %s",a[i].name);
            printf("\tTelefone: %s\n",a[i].fone);       
         }else{     
            printf("\nNao contem nenhum nome igual");
         }
    }   
}

void remover(Agenda a[]){
   printf("Qual a linha  excluir?");
    int pos;
    scanf("%d", &pos);
    pos-- ; //para ajustar aos indices que começam em 0

    if (pos >= 0 && pos < n){ 
    int i;


        for (i = pos;i < n - 1; ++i){ 
            a[i] = a[i + 1];
        }

        a = realloc(a, --n * sizeof(Agenda)); 
    }


}




int main (void){
    Agenda p[100];
    int op;
    do{
        system("cls");
        printf("--------------------------MENU-------------------------\n");
        printf("----------------- 1 para incluir contato---------------\n");
        printf("----------------- 2 para mostrar todos os contatos-----\n");
        printf("----------------- 3 para buscar por nome---------------\n");
        printf("----------------- 4 para buscar por telefone-----------\n");
        printf("----------------- 5 para remover uma linha-------------\n");
        printf("----------------- 0 para sair--------------------------\n");
        printf("----------------------------");
        scanf("%i",&op);
        fflush(stdin);
        switch(op){
            case 1:
                novo(p);
                break;
            case 2:
                imprimir(p);
                break;
            case 3:
                buscanome(p);
                break;
            case 4 :
                buscafone(p);
                break;
            case 5 :
                remover(p);
                break;
            default:
                break;
        }
    }while(op!=0);
  }
}
```

<aside class="notice">
  <strong>Aula Fundamentos Programação</strong>
  <p>
    Construa um programa que define uma estrutura de contatos, que poderá ser usado para outros programas, deverá conter atributos como: nome e telefone. Também uma função entrada de dados e uma função de saídas de dados.
  </p>
</aside>

<%= exercise_source 'ex3.html' %>
