# Exercício 2

> Código fonte e resolução do problema:

```html
<%= exercise_source 'ex2.html' %>
```

```javascript
<%= exercise_source 'js/ex2.js' %>
```

```css
<%= exercise_source 'css/ex2.scss' %>
```

```c
#include <stdio.h>

int hora(int hora, int regiao);

int main(void){
  int h, r;
  printf("Digite a hora: ");
  scanf("%i",&h);
  fflush(stdin);
  printf("Digite a regiao: ");
  scanf("%i",&r);
  fflush(stdin);
  printf("Hora %i",hora(h,r));
  return 0;
}

int hora(int hora, int regiao){
  if (regiao >=0){
    return hora + regiao;
  }else{
    return hora - regiao;
  }
}
```

<aside class="notice">
  <strong>Aula 17</strong>
  <p>
    12. Faça um programa que peca a hora atual (da região) e peça o código de uma determinada localidade e então passe a hora e o código para uma função que retorna a hora naquela localidade. Os códigos seguem abaixo:
  </p>
  <ul>
    <li>1 - Rio de Janeiro     => 0 (zero)</li>
    <li>2 - Buenos Aires       => 0 (Zero)</li>
    <li>3 - Londres            => +3</li>
    <li>4 – Roma               => +4</li>
    <li>5 - Cairo              => +5</li>
    <li>6 - Moscou             => +6</li>
    <li>7 - Chicago            => -3</li>
    <li>8 - México             => -3</li>
    <li>9 - Nova Iorque        => -2</li>
    <li>10- Iugoslávia         => -2</li>
    <li>11- Cuiabá             => -1</li>
    <li>12- Ilhas São Pedro    => +1</li>
  </ul>
</aside>

<%= exercise_source 'ex2.html' %>
