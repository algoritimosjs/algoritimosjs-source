# Exercício 5

> Código fonte e resolução do problema:

```html
<%= exercise_source 'ex5.html' %>
```

```javascript
<%= exercise_source 'js/ex5.js' %>
```

```css
<%= exercise_source 'css/ex5.scss' %>
```

```c
#include <stdio.h>

int main(void){
	int n1, n2;
	printf("Digite n1: ");
	scanf("%i",&n1);
	fflush(stdin);
	printf("Digite n2: ");
	scanf("%i",&n2);
	fflush(stdin);
	
	if (n1 > n2){
		printf("N1 e maior");
	} else if (n2 > n1){
		printf("N2 e maior");
	}else{
		printf("Sao iguais");
	}
		
	return 0;
}

```

<aside class="notice">
  <strong>Aula 7 - Estrutura condicional e estrutura de seleção</strong>
  <p>
    1) Faça um programa que peça dois números e imprima o maior deles.
  </p>
</aside>

<%= exercise_source 'ex5.html' %>
