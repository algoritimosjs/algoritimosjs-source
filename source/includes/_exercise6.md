# Exercício 6

> Código fonte e resolução do problema:

```html
<%= exercise_source 'ex6.html' %>
```

```javascript
<%= exercise_source 'js/ex6.js' %>
```

```css
<%= exercise_source 'css/ex6.scss' %>
```

```c
#include <stdio.h>

int main(void){
	int numero;
	printf("Digite o numero: ");
	scanf("%i",&numero);
	fflush(stdin);
	if (numero == 0){
		printf("numero e neutro");
	}else if (numero > 0){
		printf("numero positivo");
	}else{
		printf("numero negativo");
	}

	return 0;
}

```

<aside class="notice">
  <strong>Aula 7 - Estrutura condicional e estrutura de seleção</strong>
  <p>
    2) Faça um programa que peça um valor e mostre na tela se o valor é positivo ou negativo.
  </p>
</aside>

<%= exercise_source 'ex6.html' %>
