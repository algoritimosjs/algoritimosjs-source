# Exercício 4

> Código fonte e resolução do problema:

```html
<%= exercise_source 'ex4.html' %>
```

```javascript
<%= exercise_source 'js/ex4.js' %>
```

```css
<%= exercise_source 'css/ex4.scss' %>
```

```c
#include <stdio.h>

void maiuscula(char palavra[]);
void minuscula(char palavra[]);
void inverter(char palavra[]);
int tamanho(char palavra[]);

int main(void){
	char opcao, palavra[50];
	int i;
	do{
		system("cls");
		printf("a) - Todas as letras em maiuscula\n");
		printf("b) - Todas as letras em minuscula\n");
		printf("c) - Inverte palavra\n");
		printf("d) - Tamanho da palavra\n");
		printf("e) - Fim\n");
		printf("Digite um opcao: ");
		scanf("%c",&opcao);
		fflush(stdin);
		if (opcao != 'e'){
			printf("Digite uma palavra: ");
			gets(palavra);
			fflush(stdin);
		}
		switch(opcao){
			case 'a':
				maiuscula(palavra);
				getch();
				break;
			case 'b':
				minuscula(palavra);
				getch();
				break;
			case 'c':
				inverter(palavra);
				getch();
				break;
			case 'd':
				printf("%i",tamanho(palavra));
				getch();
				break;
		}
	}while(opcao!='e');
	return 0;
}

void maiuscula(char palavra[]){
	int i;
	for(i=0;i<50;i++){
		if(palavra[i]>='a' && palavra[i]<='z'){
			palavra[i]-=32;
		}
	}
	printf("%s",palavra);
}

void minuscula(char palavra[]){
	int i;
	for(i=0;i<50;i++){
		if(palavra[i]>='A' && palavra[i]<='Z'){
			palavra[i]+=32;
		}
	}
	printf("%s",palavra);
}

void inverter(char palavra[]){
	int i, t = tamanho(palavra);
	for(i=t-1;i>=0;i--){
		printf("%c",palavra[i]);
	}
}

int tamanho(char palavra[]){
	int i=0;
	while(palavra[i]!='\0'){
		i++;
	}
	return i;
}
```

<aside class="notice">
  <strong>Aula 17</strong>
  <p>
    11. Faça um programa que leia uma palavra e execute a opção escolhida pelo usuário conforme o menu que deve ser mostrado na tela:
  </p>
  <ul>
    <li>a) - Todas as letras em maiúscula;</li>
    <li>b) - Todas as letras em minúscula;</li>
    <li>c) - Inverte palavra;</li>
    <li>d) - Tamanho da palavra;</li>
    <li>e) - Fim;</li>
  </ul>
</aside>

<%= exercise_source 'ex4.html' %>
