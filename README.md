# Ferramentas Web - Trabalho 1

Esse é o código fonte utilizado para a seguinte aplicação: https://algoritimosjs.bitbucket.io

## Como adicionar novos exemplos

Para isso crie um branch a partir do master
```
[master]$ git checkout -b exercicio2
[exercicio2]$ git commit
[exercicio2]$ git commit
[exercicio2]$ git push -u origin exercicio2
```
Feito isso crie um "Pull Request" a partir desse novo branch

### Organização do projeto

Os exercícios novos deve ser adicionados no diretório `source/exercises` conforme a estrutura do exercicio1(ex1)

```
source/exercises/
├── _ex1.html
├── css
│   └── _ex1.scss
└── js
    └── _ex1.js
```

Existe um arquivo Markdown para cada exercício. Ele pode ser localizado em `source/includes`.

```
source/includes/
├── _example.md
└── _exercise1.md
```
Utilize arquivo `source/includes/_exercise1.md` como referencia. Sugiro duplicar e alterar o conteúdo(`cp source/includes/_exercise1.md source/includes/_exercise2.md`).

Feito isso basta incluir esse novo arquivo através do arquivo `source/index.html.md`:
```yaml
includes:
  - exercise1
  - exercise2
```

Os javascripts de `source/exercises/js/*.js` são incluidos automaticamente no `all.js`. Porem os stylesheets precisam ser incluido manualmente. Para isso basta editar o arquivo `source/screen.css.scss` e adicionar um novo `@import` com o caminho do arquivo do exercício.

## Prerequisites

You're going to need:

 - **Linux or OS X** — Windows may work, but is unsupported.
 - **Ruby, version 2.3.1 or newer**
 - **Bundler** — If Ruby is already installed, but the `bundle` command doesn't work, just run `gem install bundler` in a terminal.

### Getting Set Up
```shell
# either run this to run locally
bundle install
bundle exec middleman server

# OR run this to run with vagrant
vagrant up
```

You can now see the docs at http://localhost:4567. Whoa! That was fast!

Now that Slate is all set up on your machine, you'll probably want to learn more about [editing Slate markdown](https://github.com/lord/slate/wiki/Markdown-Syntax).

If you'd prefer to use Docker, instructions are available [in the wiki](https://github.com/lord/slate/wiki/Docker).

### Note on JavaScript Runtime

For those who don't have JavaScript runtime or are experiencing JavaScript runtime issues with ExecJS, it is recommended to add the [rubyracer gem](https://github.com/cowboyd/therubyracer) to your gemfile and run `bundle` again.

### Deploy

```
$ bundle exec middleman build [--clean]
$ cd build
$ git add .
$ git commit -m 'Release #<NUMBER>'
$ git push origin master
```
