# Trabalho - Ferramentas Web

## Tema escolhido
Resolver altoritimos da matéria de *Fundamentos a Programação* escritos em *ANSI C* usando Javascript

## Link para o Projeto
https://algoritimosjs.bitbucket.io

## Link para o Repositório GIT
https://bitbucket.org/algoritimosjs/algoritimosjs-source

## Equipe

* Ariel Sam Ribeiro
* Guilherme Sandmann
* Lucas Trancoso
* Marcos G. Zimmermann

## Professor

Glauco Vinicius Scheffel

## Instituição

Centro Universitário: Católica de Santa Catarina
